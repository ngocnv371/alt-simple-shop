﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Data
{
    public interface IItem
    {
        int Id { get; set; }
        string Title { get; set; }
        int SellingRate { get; set; }
        IType Type { get; set; }
        IList<IColor> Colors { get; set; }
        IList<ISize> Sizes { get; set; }
    }
}
