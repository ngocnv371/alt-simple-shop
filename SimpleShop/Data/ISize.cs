﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Data
{
    public interface ISize
    {
        int Id { get; set; }
        string Title { get; set; }
    }
}
