﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Data
{
    public interface IInventoryItem
    {
        int ItemId { get; set; }
        int ColorId { get; set; }
        int SizeId { get; set; }
    }
}
