﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Core;

namespace SimpleShop.Shop
{
    delegate void DoSomething();
    class MenuItem
    {
        public string Title { get; set; }
        public Core.ICommand OnSelect { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }

    public class MainMenu: IMenu
    {
        IList<MenuItem> menuItems;

        public MainMenu()
        {
            menuItems = new List<MenuItem>()
            {
                new MenuItem { Title = "Create item type", OnSelect = new Command.CreateItemTypeCommand() },
                new MenuItem { Title = "List all item types", OnSelect = new Command.ListItemTypesCommand() },
                new MenuItem { Title = "Create item color", OnSelect = new Command.CreateColorCommand() },
                new MenuItem { Title = "List all item colors", OnSelect = new Command.ListColorsCommand() },
                new MenuItem { Title = "Create item size", OnSelect = new Command.CreateSizeCommand() },
                new MenuItem { Title = "List all item sizes", OnSelect = new Command.ListItemSizesCommand() },
                new MenuItem { Title = "Create supplier", OnSelect = new Command.CreateSupplierCommand() },
                new MenuItem { Title = "List all suppliers", OnSelect = new Command.ListSuppliersCommand() },
                new MenuItem { Title = "Create item", OnSelect = new Command.CreateItemCommand() },
                new MenuItem { Title = "List all items", OnSelect = new Command.ListItemsCommand() },
                new MenuItem { Title = "Order items from supplier", OnSelect = new Command.BuyItemFromSupplierCommand() },
                new MenuItem { Title = "List inventory", OnSelect = new Command.ListInventoryCommand() },
                new MenuItem { Title = "Sell item to customer", OnSelect = new Command.SellToCustomerCommand() },
            };
            AddSampleData();
        }

        void AddSampleData()
        {
            var shirt = new Type { Id = Identity.Instance.CreateNextIdentity(), Title = "Shirt" };
            var shoes = new Type { Id = Identity.Instance.CreateNextIdentity(), Title = "Shoes" };
            var pants = new Type { Id = Identity.Instance.CreateNextIdentity(), Title = "Pants" };
            Store.Instance.Types.Add(shirt);
            Store.Instance.Types.Add(shoes);
            Store.Instance.Types.Add(pants);

            var red = new Color { Id = Identity.Instance.CreateNextIdentity(), Title = "Red" };
            var green = new Color { Id = Identity.Instance.CreateNextIdentity(), Title = "Green" };
            var blue = new Color { Id = Identity.Instance.CreateNextIdentity(), Title = "Blue" };
            Store.Instance.Colors.Add(red);
            Store.Instance.Colors.Add(green);
            Store.Instance.Colors.Add(blue);

            var x = new Size { Id = Identity.Instance.CreateNextIdentity(), Title = "X" };
            var xl = new Size { Id = Identity.Instance.CreateNextIdentity(), Title = "XL" };
            var xxl = new Size { Id = Identity.Instance.CreateNextIdentity(), Title = "XXL" };
            Store.Instance.Sizes.Add(x);
            Store.Instance.Sizes.Add(xl);
            Store.Instance.Sizes.Add(xxl);

            var dragon = new Supplier { Id = Identity.Instance.CreateNextIdentity(), Title = "Dragon Co Ltd." };
            var monkey = new Supplier { Id = Identity.Instance.CreateNextIdentity(), Title = "Monkey Fashion" };
            var jungle = new Supplier { Id = Identity.Instance.CreateNextIdentity(), Title = "Jungle Fashion" };
            Store.Instance.Suppliers.Add(dragon);
            Store.Instance.Suppliers.Add(monkey);
            Store.Instance.Suppliers.Add(jungle);

            var blueWinterDress = new Item
            {
                Id = Identity.Instance.CreateNextIdentity(),
                Title = "Blue winter dress",
                SellingRate = 6,
                Type = shirt,
                Colors = new List<Data.IColor> { blue, green },
                Sizes = new List<Data.ISize> { x, xl },
            };
            var joggingPants = new Item
            {
                Id = Identity.Instance.CreateNextIdentity(),
                Title = "Jogging Pants",
                SellingRate = 12,
                Type = pants,
                Colors = new List<Data.IColor> { red, green },
                Sizes = new List<Data.ISize> { xxl, xl },
            };
            Store.Instance.Items.Add(blueWinterDress);
            Store.Instance.Items.Add(joggingPants);

            Store.Instance.Inventory[new InventoryItem
            {
                ColorId = red.Id,
                SizeId = x.Id,
                ItemId = joggingPants.Id
            }] = 45;
            Store.Instance.Inventory[new InventoryItem
            {
                ColorId = blue.Id,
                SizeId = xl.Id,
                ItemId = blueWinterDress.Id
            }] = 7;
        }

        public void Show()
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine("-     Simple Clothing Shop     -");
            Console.WriteLine("--------------------------------");
            var item = Prompt.ReadSelectionFromList(menuItems, "");
            if (item.OnSelect != null)
            {
                item.OnSelect.Execute();
            }
            // repeat
            Show();
        }
    }
}
