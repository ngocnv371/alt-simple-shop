﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop
{
    // virtual database
    public class Store
    {
        private static Store _instance;
        public static Store Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Store();
                }
                return _instance;
            }
        }
        private Store() { }

        public IList<Data.IType> Types = new List<Data.IType>();
        public IList<Data.ISupplier> Suppliers = new List<Data.ISupplier>();
        public IList<Data.IColor> Colors = new List<Data.IColor>();
        public IList<Data.IItem> Items = new List<Data.IItem>();
        public IList<Data.ISize> Sizes = new List<Data.ISize>();
        public IDictionary<Data.IInventoryItem, int> Inventory = new Dictionary<Data.IInventoryItem, int>();
    }
}
