﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop
{
    class Identity
    {
        private static Identity _instance;
        public static Identity Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Identity();
                }
                return _instance;
            }
        }
        private Identity() { }

        int value = 0;

        public int CreateNextIdentity()
        {
            return ++value;
        }
    }
}
