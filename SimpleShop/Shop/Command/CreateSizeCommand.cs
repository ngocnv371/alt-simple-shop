﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class CreateSizeCommand : Core.ICommand
    {
        public void Execute()
        {
            var reader = new Reader.SizeReader();
            var obj = reader.Read();
            Store.Instance.Sizes.Add(obj);
            Console.WriteLine("Added new Size: {0}", obj.Title);
        }
    }
}
