﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class SellToCustomerCommand : Core.ICommand
    {
        public void Execute()
        {
            var item = Prompt.ReadSelectionFromList(Store.Instance.Items, "Select item: ");
            var color = Prompt.ReadSelectionFromList(Store.Instance.Colors, "Select color: ");
            var size = Prompt.ReadSelectionFromList(Store.Instance.Sizes, "Select size: ");
            var amount = Prompt.ReadInt("Enter quantity: ");
            Console.WriteLine("You're selling {0} x {1} ({2}, {3}) for {4}", amount, item.Title, color, size, amount * item.SellingRate);
            var inventoryItem = new InventoryItem
            {
                ItemId = item.Id,
                ColorId = color.Id,
                SizeId = size.Id
            };
            if (!Store.Instance.Inventory.ContainsKey(inventoryItem))
            {
                Console.WriteLine("You don't have that item in stock");
            }
            else if (Store.Instance.Inventory[inventoryItem] < amount)
            {
                Console.WriteLine("You don't have enough of that item in stock");
            }
            else
            {
                Store.Instance.Inventory[inventoryItem] -= amount;
            }
        }
    }
}
