﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class CreateSupplierCommand : Core.ICommand
    {
        public void Execute()
        {
            var reader = new Reader.SupplierReader();
            var obj = reader.Read();
            Store.Instance.Suppliers.Add(obj);
            Console.WriteLine("Added new Supplier: {0}", obj.Title);
        }
    }
}
