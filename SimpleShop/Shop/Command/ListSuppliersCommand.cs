﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListSuppliersCommand : Core.ICommand
    {
        public void Execute()
        {
            Console.WriteLine("All Suppliers in the system:");
            foreach (var obj in Store.Instance.Suppliers)
            {
                Console.WriteLine(obj);
            }
        }
    }
}
