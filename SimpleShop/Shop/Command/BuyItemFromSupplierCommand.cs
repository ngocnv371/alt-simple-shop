﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class BuyItemFromSupplierCommand : Core.ICommand
    {
        public void Execute()
        {
            var supplier = Prompt.ReadSelectionFromList(Store.Instance.Suppliers, "Select supplier: ");
            var item = Prompt.ReadSelectionFromList(Store.Instance.Items, "Select item: ");
            var color = Prompt.ReadSelectionFromList(Store.Instance.Colors, "Select color: ");
            var size = Prompt.ReadSelectionFromList(Store.Instance.Sizes, "Select size: ");
            var amount = Prompt.ReadInt("Enter quantity: ");
            var rate = Prompt.ReadInt("Enter buying rate: ");
            Console.WriteLine("You're buying {0} x {1} ({2}, {3}) from {4} at rate {5}", amount, item.Title, color, size, supplier, rate);
            Console.WriteLine("You have to pay: {0}", amount * rate);
            var inventoryItem = new InventoryItem
            {
                ItemId = item.Id,
                ColorId = color.Id,
                SizeId = size.Id
            };
            if (!Store.Instance.Inventory.ContainsKey(inventoryItem))
            {
                Store.Instance.Inventory[inventoryItem] = amount;
            }
            else
            {
                Store.Instance.Inventory[inventoryItem] += amount;
            }
        }
    }
}
