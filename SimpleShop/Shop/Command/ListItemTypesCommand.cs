﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListItemTypesCommand : Core.ICommand
    {
        public void Execute()
        {
            Console.WriteLine("All types in the system:");
            foreach (var type in Store.Instance.Types)
            {
                Console.WriteLine(type);
            }
        }
    }
}
