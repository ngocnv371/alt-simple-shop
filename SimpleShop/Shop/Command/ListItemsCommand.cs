﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListItemsCommand : Core.ICommand
    {
        public void Execute()
        {
            Console.WriteLine("All items in the system:");
            foreach (var obj in Store.Instance.Items)
            {
                Console.WriteLine(obj);
            }
        }
    }
}
