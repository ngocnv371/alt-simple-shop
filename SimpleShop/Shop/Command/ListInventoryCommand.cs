﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListInventoryCommand : Core.ICommand
    {
        public void Execute()
        {
            foreach (var pair in Store.Instance.Inventory)
            {
                var item = Store.Instance.Items.First(i => i.Id == pair.Key.ItemId);
                var color = Store.Instance.Colors.First(c => c.Id == pair.Key.ColorId);
                var size = Store.Instance.Sizes.First(s => s.Id == pair.Key.SizeId);
                Console.WriteLine("{0} x {1} ({2}, {3})", pair.Value, item.Title, color, size);
            }
        }
    }
}
