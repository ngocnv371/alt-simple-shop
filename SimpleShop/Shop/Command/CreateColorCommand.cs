﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class CreateColorCommand : Core.ICommand
    {
        public void Execute()
        {
            var reader = new Reader.ColorReader();
            var obj = reader.Read();
            Store.Instance.Colors.Add(obj);
            Console.WriteLine("Added new Color: {0}", obj.Title);
        }
    }
}
