﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class CreateItemCommand : Core.ICommand
    {
        public void Execute()
        {
            var reader = new Reader.ItemReader();
            var obj = reader.Read();
            Store.Instance.Items.Add(obj);
            Console.WriteLine("Added new item: {0}", obj.Title);
        }
    }
}
