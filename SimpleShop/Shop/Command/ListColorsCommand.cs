﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListColorsCommand : Core.ICommand
    {
        public void Execute()
        {
            Console.WriteLine("All Colors in the system:");
            foreach (var obj in Store.Instance.Colors)
            {
                Console.WriteLine(obj);
            }
        }
    }
}
