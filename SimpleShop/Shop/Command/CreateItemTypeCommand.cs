﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class CreateItemTypeCommand : Core.ICommand
    {
        public void Execute()
        {
            var reader = new Reader.TypeReader();
            var type = reader.Read();
            Store.Instance.Types.Add(type);
            Console.WriteLine("Added new type: {0}", type.Title);
        }
    }
}
