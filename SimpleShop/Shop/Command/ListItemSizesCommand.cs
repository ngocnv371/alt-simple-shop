﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop.Command
{
    public class ListItemSizesCommand : Core.ICommand
    {
        public void Execute()
        {
            Console.WriteLine("All Sizes in the system:");
            foreach (var obj in Store.Instance.Sizes)
            {
                Console.WriteLine(obj);
            }
        }
    }
}
