﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop.Reader
{
    public class SizeReader : Data.IDataReader<Data.ISize>
    {
        public ISize Read()
        {
            Console.WriteLine("---------SizeReader-----------");
            var name = Prompt.ReadString("Enter size name: ");
            var obj = new Size
            {
                Title = name
            };
            return obj;
        }
    }
}
