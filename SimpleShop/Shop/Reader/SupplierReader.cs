﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop.Reader
{
    public class SupplierReader : Data.IDataReader<Data.ISupplier>
    {
        public ISupplier Read()
        {
            Console.WriteLine("---------SupplierReader-----------");
            var name = Prompt.ReadString("Enter supplier name: ");
            var obj = new Supplier
            {
                Title = name
            };
            return obj;
        }
    }
}
