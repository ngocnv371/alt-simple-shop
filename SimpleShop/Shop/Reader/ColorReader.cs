﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop.Reader
{
    public class ColorReader : Data.IDataReader<Data.IColor>
    {
        public IColor Read()
        {
            Console.WriteLine("---------ColorReader-----------");
            var name = Prompt.ReadString("Enter color name: ");
            var obj = new Color
            {
                Title = name
            };
            return obj;
        }
    }
}
