﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop.Reader
{
    public class ItemReader : Data.IDataReader<Data.IItem>
    {
        public IItem Read()
        {
            Console.WriteLine("---------ItemReader-----------");
            var name = Prompt.ReadString("Enter item name: ");
            var obj = new Item
            {
                Title = name
            };
            obj.SellingRate = Prompt.ReadInt("Enter selling rate: ");
            obj.Type = Prompt.ReadSelectionFromList(Store.Instance.Types, "Select type: ");
            obj.Colors = Prompt.ReadMultipleSelectionsFromList(Store.Instance.Colors, "Select color(s): ");
            obj.Sizes = Prompt.ReadMultipleSelectionsFromList(Store.Instance.Sizes, "Select sizes(s): ");
            return obj;
        }
    }
}
