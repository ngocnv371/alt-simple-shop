﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop.Reader
{
    public class TypeReader : Data.IDataReader<Data.IType>
    {
        public IType Read()
        {
            Console.WriteLine("---------TypeReader-----------");
            var name = Prompt.ReadString("Enter item type name: ");
            var type = new Type
            {
                Title = name
            };
            return type;
        }
    }
}
