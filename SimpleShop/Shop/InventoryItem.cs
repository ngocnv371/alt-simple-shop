﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop
{
    public class InventoryItem: Data.IInventoryItem
    {
        public int ItemId { get; set; }
        public int ColorId { get; set; }
        public int SizeId { get; set; }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", ItemId, ColorId, SizeId);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals(obj.ToString());
        }
    }
}
