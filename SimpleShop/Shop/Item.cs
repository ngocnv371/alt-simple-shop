﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleShop.Data;

namespace SimpleShop.Shop
{
    public class Item: Data.IItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int SellingRate { get; set; }
        public IType Type { get; set; }
        public IList<IColor> Colors { get; set; }
        public IList<ISize> Sizes { get; set; }

        public Item()
        {
            Colors = new List<IColor>();
            Sizes = new List<ISize>();
        }

        public override string ToString()
        {
            var text = string.Empty;
            text += string.Format("{0} ${1} {2}", Title, SellingRate, Type);
            text += string.Format(" (sizes: {0}) (colors: {1})", JoinString(Sizes, ", "), JoinString(Colors, ", "));
            return text;
        }

        string JoinString<T>(IList<T> list, string delimiter)
        {
            var text = string.Empty;
            foreach (var item in list)
            {
                text += (item + delimiter);
            }
            for (var i = delimiter.Length - 1; i >= 0; i--)
            {
                text = text.TrimEnd(delimiter[i]);
            }
            return text;
        }
    }
}
