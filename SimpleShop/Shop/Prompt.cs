﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop.Shop
{
    public static class Prompt
    {
        public static int ReadInt(string prompt)
        {
            Console.Write(prompt);
            var line = Console.ReadLine();
            int val = -1;
            if (int.TryParse(line, out val))
            {
                return val;
            }
            else
            {
                Console.WriteLine("Please enter a valid integer");
                return ReadInt(prompt);
            }
        }

        public static string ReadString(string prompt)
        {
            Console.Write(prompt);
            var line = Console.ReadLine();
            if (line.Trim().Length > 0)
            {
                return line.Trim();
            }
            else
            {
                Console.WriteLine("Please enter a non-empty string");
                return ReadString(prompt);
            }
        }

        public static T ReadSelectionFromList<T>(IList<T> list, string prompt)
        {
            Console.WriteLine(prompt);
            for (var i = 0; i < list.Count; i++)
            {
                Console.WriteLine("[{0}] {1}", i, list[i]);
            }
            var index = ReadInt("Enter menu index to select: ");
            if (index >= 0 && index < list.Count)
            {
                return list[index];
            }
            else
            {
                Console.WriteLine("Please enter a valid menu index");
                return ReadSelectionFromList(list, prompt);
            }
        }

        public static IList<T> ReadMultipleSelectionsFromList<T>(IList<T> list, string prompt)
        {
            Console.WriteLine(prompt);
            for (var i = 0; i < list.Count; i++)
            {
                Console.WriteLine("[{0}] {1}", i, list[i]);
            }
            var line = ReadString("Enter menu indices separate by ',' to select multiple items: ");
            var items = line.Split(',').Select(int.Parse).Select(idx => list[idx]);
            return items.ToList();
        }
    }
}
