﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShop
{
    public class App
    {
        public void Run()
        {
            var menu = new Shop.MainMenu();
            menu.Show();
            Console.ReadKey();
        }
    }
}
